﻿namespace Kilian
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public static class EmptyStaticClassBonus
    {
        public static float ValueA = 2f;
        public static float ValueB = 4f;


        public static void SetValueA(float valueA)
        {
            ValueA = valueA;
        }

        public static void SetValueB(float valueB)
        {
            ValueB = valueB;
        }
    }

}